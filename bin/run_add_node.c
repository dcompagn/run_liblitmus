#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "litmus.h"
#include "common.h"

static void usage(char *error) {
	fprintf(stderr, "Error: %s\n", error);
	fprintf(stderr,
		"Usage:\n"
		"	run_add_set ID RATE_A RATE_B LEVEL\n"
		"\n");
	exit(EXIT_FAILURE);
}

int main(int argc, char** argv)
{
	int ret = 0;
	int id, level;
	lt_t rate_a, rate_b;

	if (argc < 5)
		usage("Arguments missing.");

	id = atoi(argv[1]);
	rate_a = atoll(argv[2]);
	rate_b = atoll(argv[3]);
	level = atoi(argv[4]);

	ret = run_add_node(&id, &rate_a, &rate_b, &level);

	if (ret != 0)
		bail_out("Could not add the new node.");

	printf("run_add_node %d %llu %llu %d\n", id, rate_a, rate_b, level);

	return 0;
}
